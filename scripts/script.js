"use strict";

/**
 * @author Emilia Krichevsky #1941891
 */


document.addEventListener("DOMContentLoaded", setup);

/**
 * global object to access html elements without 
 * having to retrive them constantly within each function
 */
let access = {};
  

/**
 * is called when DOM loaded event is fired, adds the APIs url, 
 * and the HTMLs paragraphs to the global object
 * adds an 'click' event listener to the button in HTML
 * 
 */
function setup(){
    document.querySelector("#getbtn").addEventListener("click", getInfo);
    access.apiURL = "https://ron-swanson-quotes.herokuapp.com/v2/quotes";
    access.para = document.querySelector("#quote");
    access.err = document.querySelector("#err"); 
}

/**when click event is fired:
 * construct the URL and call function to retrive json data
 * 
 * @param {*} e 
 */
function getInfo(e){

    let url = generateURL();
    fetchQuote(url, displayQuote, displayError);
}

/**
 * constructor that generates the API's URL
 */
function generateURL(){
    let url = new URL(access.apiURL);
    console.log(url);
    return url;
}

/**
 * uses fetch to retrieve json data from API
 * if  response is ok, call displayQuote
 * if anything else, call displayError
 * 
 * @param {string} url
 * @param {funct to display quote} displayQuote
 * @param {func to display error} displayError
 */

function fetchQuote(url, displayQuote, displayError){

    fetch(url)
    .then( response => {
        if(!response.ok) { 
            throw new Error('Status code: ' + response.status); // throw error
        }
        else {
            return response.json();    
        }
    })
    .then (data => displayQuote(data))
    
    .catch( error => displayError(error));
}

/**
 * displays the error to the user and asks them to try again
 * hides paragraph with quote and replaces it with error message paragraph
 * 
 * @param {thrown error} msg 
 */

function displayError(msg){
    console.error(msg);
    access.para.style.visibility = "hidden";
    access.err.style.visibility = "visible";
    access.err.textContent = "Oops! Something went wrong, Please try again.";  
}

/**
 * takes JSON data and displays it into the DOM
 * hides previous error messages
 * 
 * @param {JSON} json 
 */

function displayQuote(json){

    access.err.style.visibility = "hidden";
    access.para.style.visibility = "visible";
    access.para.textContent =  json;

}